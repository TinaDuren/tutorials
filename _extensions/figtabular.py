"""
Adds a new directive called 'figtable' that creates a figure
around a table.
"""

from docutils import nodes
import docutils.parsers.rst.directives as directives
from docutils.parsers.rst import Directive
from sphinx import addnodes

class figtabular(nodes.General, nodes.Element):
    pass

def visit_figtabular_node(self, node):
    pass

def depart_figtabular_node(self, node):
    pass

def visit_figtabular_tex(self, node):
    if node['nofig']:
        self.body.append('\n\n\\capstart\n\\begin{center}\n')
    else:
        self.body.append('\n\n\\capstart\n\\begin{center}\n')

def depart_figtabular_tex(self, node):
    if node['nofig']:
        self.body.append('\n\\end{center}\n\n')
    else:
        self.body.append('\n\\end{center}\n\n')

def visit_figtabular_html(self, node):
    atts = {'class': 'figure align-center'}
    self.body.append(self.starttag(node, 'div', **atts) + '<center>')

def depart_figtabular_html(self, node):
    self.body.append('</center></div>')

class FigTabularDirective(Directive):

    has_content = True
    optional_arguments = 5
    final_argument_whitespace = True

    option_spec = {'label': directives.uri,
                   'spec': directives.unchanged,
                   'caption': directives.unchanged,
                   'alt': directives.unchanged,
                   'nofig': directives.flag}

    def run(self):
        label = self.options.get('label', None)
        spec = self.options.get('spec', None)
        caption = self.options.get('caption', None)
        alt = self.options.get('alt', None)
        nofig = 'nofig' in self.options

        figtabular_node = figtabular('', ids=[label] if label is not None else [])
        figtabular_node['nofig'] = nofig

        if spec is not None:
            table_spec_node = addnodes.tabular_col_spec()
            table_spec_node['spec'] = spec
            figtabular_node.append(table_spec_node)

        node = nodes.Element()
        self.state.nested_parse(self.content, self.content_offset, node)
        tablenode = node[0]
        if alt is not None:
            tablenode['alt'] = alt
        figtabular_node.append(tablenode)

        if caption is not None:
            caption_node = nodes.caption('', '', nodes.Text(caption))
            figtabular_node.append(caption_node)

        if label is not None:
            targetnode = nodes.target('', '', ids=[label])
            figtabular_node.append(targetnode)

        return [figtabular_node]

def setup(app):
    app.add_node(figtabular,
                 html=(visit_figtabular_html, depart_figtabular_html),
                 singlehtml=(visit_figtabular_html, depart_figtabular_html),
                 latex=(visit_figtabular_tex, depart_figtabular_tex),
                 text=(visit_figtabular_node, depart_figtabular_node))

    app.add_directive('figtabular', FigTabularDirective)
