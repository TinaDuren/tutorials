from docutils import nodes
from docutils.parsers.rst import Directive, directives
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils.parsers.rst.directives.misc import Class
from docutils.parsers.rst.directives.misc import Include as BaseInclude
from sphinx.locale import _

from sphinx import addnodes

from smallcaps import smallcaps
from emails import email, is_email


NBS = "\xa0"

class authorsec(nodes.Part, nodes.TextElement):
    pass

def visit_authorsec_html(self, node):
    self.body.append(r'<span style="font-size:80%;font-style:italic;float:right">')

def depart_authorsec_html(self, node):
    self.body.append(r'</span><br>')

def visit_authorsec_tex(self, node):
    self.body.append(r'\hfill\begin{minipage}[t]{0.85\textwidth}\begin{flushright}\begin{spacing}{0.9}{\itshape\sffamily\scriptsize ')

def depart_authorsec_tex(self, node):
    self.body.append(r'}\end{spacing}\end{flushright}\end{minipage}')

def format_author(author, self, sc=True):
    lst = []
    a_lst = author.split()
    l = len(a_lst)
    if l == 0: return []

    val = None
    nxt = a_lst[0].strip()
    for i, el in enumerate(a_lst):
        el = el.strip()
        val = nxt
        nxt = a_lst[i+1].strip() if ((i+1) < l) else None
        if sc and is_email(nxt):
            # Last name is small caps
            n = smallcaps()
            n += nodes.Text(val)
            lst += [n, nodes.Text(NBS)]
        elif is_email(val):
            # i.e. is the email address
            n = email(val, val)
            #n = nodes.Text(val)
            lst += [n]
        else:
            n = nodes.Text(val)
            lst += [n, nodes.Text(NBS)]
    return lst



class SectionAuthor(Directive):
    has_content = False
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    option_spec = {} # type: Dict

    def run(self):
        # type: () -> List[nodes.Node]
        authors = [format_author(el, self) for el in self.arguments[0].replace("\n", ",").split(",")]
        if len(authors) > 1:
            *authors, last_author = authors
            sep = nodes.Text(", ")
            last_sep = nodes.Text(" and\xa0")
        else:
            last_author = nodes.Text("")
            sep = nodes.Text("")
            last_sep = sep

        para = nodes.paragraph(translatable=False)
        span = authorsec()
        para += span
        #para += intro
        for i, author in enumerate(authors):
            if i > 0:
                span += sep
            span += author
        span += last_sep
        span += last_author
        # Finally create the node
        return [para]

def setup(app):
    app.add_node(authorsec,
                 html=(visit_authorsec_html, depart_authorsec_html),
                 latex=(visit_authorsec_tex, depart_authorsec_tex),
                 )
    app.add_directive('secauthor', SectionAuthor)
