from docutils import nodes, utils
from docutils.parsers.rst import Directive, directives
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils.parsers.rst.directives.misc import Class
from docutils.parsers.rst.directives.misc import Include as BaseInclude
from sphinx.locale import _

from os import path
from sphinx import addnodes
from sphinx.util.nodes import split_explicit_title
from sphinx.util.fileutil import copy_asset, copy_asset_file


import os
def copy_asset_file(source, destination, context=None, renderer=None):
    # type: (unicode, unicode, Dict, BaseRenderer) -> None
    """Copy an asset file to destination.
    On copying, it expands the template variables if context argument is given and
    the asset is a template file.
    :param source: The path to source file
    :param destination: The path to destination file or directory
    :param context: The template variables.  If not given, template files are simply copied
    :param renderer: The template engine.  If not given, SphinxRenderer is used by default
    """
    if not os.path.exists(source):
        print("no source", source)
        return

    print("src ok")
    if os.path.exists(destination) and os.path.isdir(destination):
        # Use source filename if destination points a directory
        destination = os.path.join(destination, os.path.basename(source))

    if source.lower().endswith('_t') and context:
        if renderer is None:
            from sphinx.util.template import SphinxRenderer
            renderer = SphinxRenderer()

        with codecs.open(source, 'r', encoding='utf-8') as fsrc:  # type: ignore
            if destination.lower().endswith('_t'):
                destination = destination[:-2]
            with codecs.open(destination, 'w', encoding='utf-8') as fdst:  # type: ignore
                fdst.write(renderer.render_string(fsrc.read(), context))
    else:
        copyfile(source, destination)

class dl(addnodes.download_reference):
    pass

def visit_dl_html(self, node):
    self.visit_download_reference(node)

def depart_dl_html(self, node):
    self.depart_download_reference(node)

def visit_dl_tex(self, node):
    src = path.join(self.builder.srcdir, "_pages", node["reftarget"])
    # print("dlfiles:", self.builder.env.dlfiles)
    print("IMG:", self.builder.images)
    self.builder.config.latex_additional_files.append(src)
    fname = path.basename(node["reftarget"])
    self.body.append(r'\textattachfile[color=0.208 0.374 0.486]{%s}{'%fname)

def depart_dl_tex(self, node):
    self.body.append(r'}')

def dl_role(typ, rawtext, text, lineno, inliner, options={}, content=[]):
    has_explicit_title, title, target = split_explicit_title(text)
    title = utils.unescape(title)
    target = utils.unescape(target)
    node = dl(rawtext, reftype="download", refdomain="",
              refexplicit=has_explicit_title)
    node['reftarget'] = target
    node += nodes.literal(rawtext, title, classes=["xref", "docutils", "literal", "download"])
    return [node], []

def setup(app):
    app.add_node(dl,
                 html=(visit_dl_html, depart_dl_html),
                 latex=(visit_dl_tex, depart_dl_tex),
                 )
    app.add_role("dl", dl_role)
