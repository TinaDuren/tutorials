
.. _prompt_primer:

More about the command prompt
=============================

.. secauthor:: Gaël Donval <g.donval@bath.ac.uk>

From now on, you will spend a lot of your time in the terminal, at the command
prompt, typing command lines.  As you saw in :numref:`ssh_success`, the command
*prompt* (where you can type command lines) can be roughly decomposed thusly:

.. code-block:: console

    [<username>@<hostname> <working directory>]$ 

The ``<username>`` should be yours, ``<hostname>`` should be either
``balena-01`` or ``balena-02`` for the time being and your working directory
should be ``~`` which is a shortcut notation for your *home directory*.


Useful conventions
------------------

Representing the full prompt each time we want you to type something in is not
really convenient. Instead, we will abide by the usual convention and
reduce the full prompt to ``$``: as you may have figured out now, ``$``
denotes the start of the area where you can input commands:

.. code-block:: console

    $ <command>

Here ``<command>`` is a placeholder representing a command to type, ``$`` just
indicates that the following command needs to be typed at the prompt; ``$``
itself shouldn't be typed.  Once the command is typed, you need to press
:kbd:`Enter` to actually execute it.

Sometimes, we also want to give you the computer's answer to a command, in 
which case the answer will be given in the same block but without ``$``:

.. code-block:: console

    $ <command>
    <answer>

For instance try the following (remember to press :kbd:`Enter` at the end
of each command):

.. code-block:: console

    $ echo "Hello World!"
    Hello World!
    $ echo "One Two One Two"
    One Two One Two

The command ``echo`` just displays back what you typed after it. Notice how you
get your prompt back after each of these commands and can start to type other
things right away.


Help, I am stuck!
-----------------

Now, as you become more and more proficient with the command line, you will
have to type in more complex command. Try the following ones:

.. code-block:: console

    $ watch -t echo "Hello World!"
    $ echo "Hello World!" | less

Their exact meaning does not matter yet. However if you typed any one of these
commands correctly, you should now be stuck: the command prompt does not appear
anymore no matter what you do or how long you wait! You will soon understand
why…

In the meanwhile you need to learn how to get the prompt back when you are
stuck: depending on the program, pressing :kbd:`q` or :kbd:`Ctrl-c` (i.e.  keep
the :kbd:`control` key pressed and press the key :kbd:`c`) should be enough to
get the command prompt back. There is no harm done in trying both if you are
unsure.


Understanding what you type
---------------------------

We understand that this is all new to you and we expect that you will need some
time to get familiar with it. However there is no point in blindly typing
things in.

Instead, we urge you to go slowly in order to ensure you understand what you
are doing (more on that in :ref:`looking_for_CL_info`).  For instance, let us
say we asked you to try to input the command, ``whoami`` giving the following
snippet:

.. code-block:: console

    $ whoami
    ggd21

As described before, we expect you to type ``whoami`` and press :kbd:`Enter`.
That specific reply is my user name. Do you expect to get the same answer?
Try it!

Most returned values we will give you in this documentation are like this: 
they are meant as relevant examples rather than specific answers.


.. _looking_for_CL_info:

What does command *X* do?
---------------------------

There are 4 main ways to know what a command is about:

* ``$ man <X>`` should display a manual page (if not, try another method).
  It should contain different sections such as ``NAME`` where the command
  is briefly described; a ``SYNOPSIS`` where the different ways to invoke
  that command are given and a ``DESCRIPTION`` containing a longer description
  and the list of possible options and flags. Press :kbd:`q` to quit.
* ``$ info <X>`` provides very similar information but the description is
  usually better than with ``man``. Again press :kbd:`q` to quit.
* ``$ <X> --help`` calls for the help page of program ``<X>`` by passing it
  the ``--help`` flag. Those help pages are usually shorter than the other
  ones: they are really useful once you are familiar with a command but are
  unsure about a specific option.
* Search for the command name on the internet. Be careful with this method
  though as the version of the program you can find on the internet might
  be different.

Now look for the definition and usage of the following commands: ``ls``,
``pwd``, ``cd``, ``whoami``, ``cp``, ``mv``, ``rm``, ``mkdir``, ``touch``,
``echo``, ``date`` and ``nano``.  We suggest that you write down that list with
the corresponding definitions for future reference: notice the similarly between
the command name and what it is used for. Square-bracketed elements are usually
optional, anything starting with a dash (``-``) or a double-dash (``--``) is
usually called a *flag* and in this particular context pipes (``|``) often
represent alternatives.

You don't need to go through all the options for all those commands now,
however if you see a command like ``ls -s`` you should feel comfortable with
consulting the man page (and any other source of information you need) to
understand how the flag ``-s`` is altering ``ls``'s behaviour! 

.. note:: Don't hesitate to experiment with ``ls`` as much as 
          you want to: that command is perfectly safe and it will give you a 
          feeling of how flags are used to tweak a program's behaviour!


.. _looking_for_new_commands:

What's the command to do *Y*?
-----------------------------

The manual pages are great resources to know how to use a command you already
know about for a given application but they are poor resources to discover new
commands. Let us suppose you didn't go through the list of commands in the
previous section and still don't know how to copy a folder. The easiest
(and recommended) way to find an answer is to look for it on the internet.
Try either of these:

.. code-block:: text

    how to copy directory command line linux

.. code-block:: text

    how to copy directory bash

You are going to be flooded with answers to that question that hundreds of
clueless people already asked in various places: *searching* for information
should be your very first reaction; *asking* for answers should be your last
recourse.

The ``bash`` I am mentioning in the second search is the specific name of
the command prompt language you will use on |Balena|. You may also
see references to the :smallcaps:`Bash shell` which is just a fancy name for
the command prompt that happens to be used on |Balena|. 

.. hint:: Appending ``bash`` as a keyword is a great way to get relevant answers.

.. warning:: You **must** always understand what you type, especially when 
    it's coming from the internet: always look at the manual page if unsure
    and read the full explanations. Most commands need adapting and
    some can damage you data.


What if something goes wrong?
-----------------------------

Learning a new interface can be overwhelming but please keep in mind that
however magical it looks, the computer is going to do exactly what you told it
to do. If something went wrong somewhere, it most certainly mean that you
misused something in the first place. Fortunately though the computer is going
to tell you when and (often) where something went wrong.  For instance if you
mistyped something and wrote a command that doesn't exist, you will see
something like this:

.. code-block:: console

    $ idontexist
    -bash: idontexist: command not found

If you try to point to an non-existing folder, you'll get something like:

.. code-block:: console

    $ ls /i_m_not_there_either
    ls: cannot access /i_m_not_there_either: No such file or directory

If you try to use an invalid option, you will get:

.. code-block:: console

    $ cp -z
    cp: invalid option -- 'z'
    Try `cp --help' for more information.

Pay attention to these: it is much more helpful for us to hear something like
"I don't understand, it says that program ``<X>`` does not exist, what should
I do?" rather than "there's an error" or even worse "it's not working". 
A specific error message usually leads to a quick solution: "it's not working" 
covers everything from minor numerical errors to your computer catching fire.


How to edit a file?
-------------------

Use ``nano`` (you should have looked for that command in 
:ref:`looking_for_CL_info` already).


How to look at file content?
----------------------------

You could use an editor like ``nano`` for this task but what is called a
*pager* would be more suitable. We recommend that you use ``less`` for that
purpose:

.. code-block:: console

    $ less <filename>

You must press :kbd:`q` to quit that program. You can search for terms by
pressing ``/<search term>`` and press :kbd:`Enter`. Then press :kbd:`n`
to go to the next occurrence of ``<search term>`` and :kbd:`Shift-n` to go
back to the previous one.

Press :kbd:`Shift-g` to reach the end of the file. If the calculation is still
running and you need to see the latest updates of ``<filename>``, press
:kbd:`Shift-f`. 

.. note:: the name ``less`` is a pun in reference to an older pager called
    ``more``. You may safely replace all occurrence of ``more`` you may find
    in documentation by ``less``. You can also dump the entirety of a short 
    file to the screen using the command ``cat``.

