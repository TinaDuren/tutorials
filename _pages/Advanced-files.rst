
.. _file_sharing:

Accessing remote files
======================

.. secauthor:: Gaël Donval <g.donval@bath.ac.uk>


Accessing from university's computers
-------------------------------------

If you are working on one of the university's computer, you can copy small
files back and forth seamlessly: your university files will be in a special
location called :file:`$BUCSHOME` (this time, the ``$`` belongs to the
name and must be typed: there is no space between it and the rest of the name).

For instance, the following should list the files you currently store on
your university computer ``My documents`` folder:

.. code-block:: console

    $ ls $BUCSHOME/dos

You can copy things back and forth using ``cp``:

.. code-block:: console

    $ nano test.txt

Type in something like: ``This is from Balena``, save and copy the file
over to your :file:`H` drive:

.. code-block:: console

    $ cp test.txt $BUCSHOME/dos/

Then have a look at your document folder: a new file called :file:`test.txt`
should have appeared. Open it with ``Notepad`` and replace ``Balena`` with
``Windows`` and save. Then type:

.. code-block:: console

    $ cp $BUCSHOME/dos/test.txt test.txt
    $ less test.txt
    This is from Windows

Accessing files from |Windows|/|Mac|
------------------------------------

If you are using |MobaXterm| as described in :ref:`connect_to_Balena`, you
should be able to drag and drop files directly within that application.

On the university computers, you can use |Filezilla|_ instead: there is a
portable version distributed as a :file:`.zip` file. Once launched
set the ``Host`` to ``balena.bath.ac.uk``, type your user name and password
and put ``22`` as ``Port``. The files on |Balena| should be displayed on the
right pane.

.. _Filezilla: https://filezilla-project.org/download.php?show_all=1

Accessing files from |Linux|
----------------------------

Under |Linux|, you can use your native file browser to access your files on
|Balena| just as if they were on you computer. Be sure to have ``gvfs``
installed. Open :prog:`Nautilus` for instance.  Press :kbd:`Ctrl-l` and type
``sftp://<username>@balena.bath.ac.uk``.  Press :kbd:`Enter`. You should now be
able to access files on |Balena| just as if they were on you computer. You can
bookmark the location and save your password for future use.


Accessing files from the command line
-------------------------------------

|Linux| and |Mac| users can use ``sftp`` or ``scp`` to pass files
around:

.. code-block:: console

    [mycomputer ~]$ ssh <username>@balena.bath.ac.uk
    [balena-01 ~]$ echo "This is from Balena" > test.txt
    [balena-01 ~]$ exit
    logout
    [mycomputer ~]$ sftp <username>@balena.bath.ac.uk:test.txt text.txt
    [mycomputer ~]$ cat text.txt
    This is from Balena

As described here, those commands are rather cumbersome to use though they 
should work as long as |OpenSSH| is there.

