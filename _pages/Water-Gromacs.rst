
.. _water_in_gromacs:

**********************************************
Calculating properties of water with |Gromacs|
**********************************************

.. secauthor:: Tina Düren <t.duren@bath.ac.uk>
               Lev Sarkisov <Lev.Sarkisov@ed.ac.uk>
               Gaël Donval <g.donval@bath.ac.uk>
			   Claire Hobday <c.l.hobday@bath.ac.uk>

In this tutorial you will use molecular dynamics simulations to investigate
the thermodynamic properties and structural characteristics of bulk water using
a molecular simulation package called |Gromacs|.

.. admonition:: Objectives
   :class: hint

    - Acquiring basic practical understanding of molecular dynamics simulations.
    - Learning how to run |MD| simulations using the program |Gromacs| 
      on the university's high performance computing cluster, |Balena|.
    - Learning how to visualize your results using |VMD|.
    - Learning about basic molecular models of water; fundamental properties of
      water, role of hydrogen bonds in the unusual behaviour of water.
    - Familiarizing yourself with key concepts of molecular modelling such as
      radial distribution functions and self-diffusion coefficient from mean
      squared displacement of molecules as function of time.

**Before you start:**

#. Review the slides on molecular dynamics simulations.
#. Review your command-line notes.
#. If you have access to the book, read chapter 29 of :smallcaps:`Dill` and
   :smallcaps:`Bromberg`, |MolDriv|_, on water properties. 

   .. |MolDriv| replace:: *Molecular Driving forces*
   .. _MolDriv: https://doi.org/10.1021/ed2005328

#. Test yourself by answering the following questions:

    * What is the geometry of a water molecule?
    * What is a hydrogen bond? How do we know that two water molecules formed a
      hydrogen bond? What is the strength of a hydrogen bond? How many hydrogen
      bonds does a single molecule of water form on average in liquid water at
      ambient conditions?
    * What are the anomalous properties of water compared to other liquids
      (such as liquid argon or methane)?



Background
----------

Properties of water are of an immense importance in both biology and various
technological applications, and at the same time they exhibit a number of
anomalous trends (different from other
fluids), that can be properly understood only within the framework of molecular
thermodynamics. The model of water we employ in this study is *simple
point charge* model (:smallcaps:`spc`) proposed by :smallcaps:`Berendsen` and
co-workers `several years ago <https://doi.org/10.1021/j100308a038>`_. The
model features a charged oxygen atom at a distance of :math:`1\,`\ Å of two
hydrogen atoms forming a :math:`\angle \mathrm{HOH}` angle of :math:`109.47`\ °
(see :numref:`water_schematics`).

.. _water_schematics:

.. figure:: img/water.*
    :width: 70%

    Schematic depiction of a water molecule

The interaction between two water molecules is described by the sum of a
Coulombic term  and a dispersive term between the oxygen atom of each molecule
only:

.. math::

    U      = \frac{1}{2} \sum_{i=1}^3 \sum_{j=1}^3
             \frac{q_i~q_j}{4\pi \varepsilon_0}
           + \frac{A}{r^{12}_\mathrm{OO}}
           - \frac{B}{r^{6}_\mathrm{OO}}

where :math:`A = 629.4 \times 10^3~\mathrm{kcal\,Å^{12}/mol}` and :math:`B =
625.5~\mathrm{kcal\,Å^{6}/mol}`. This model is reasonably accurate despite its
computational simplicity for the prediction of properties of liquid water at
ambient temperature.  On the other hand it is not very accurate in describing
liquid-solid transitions of water. For example, you can see in
:numref:`water_spc_failure` that even an improved :smallcaps:`spc` model of
water (:smallcaps:`spc/e`, right) poorly agrees with the experimental diagram
(left): it is not able to predict some of the ice phases at all, and the
liquid/ice-I (the ice we know) transition is shifted towards lower
temperatures.

.. _water_spc_failure:

.. figure:: img/water-spc-failure.*
    :width: 70%

    Phase diagram of water as obtained from the experiment (left) and from the
    :smallcaps:`spc/e` model (right). Note the shift of
    :math:`0.1\,\mathrm{GPa}` on the y-axis.  Adapted from `this
    <water_failure_ref>`_.

.. _water_failure_ref: https://doi.org/10.1103/PhysRevLett.92.255701


Anyway, let us start working on this system.



Setting up your account on |Balena|
-----------------------------------

.. note:: This is a one-off setup step that you have to do as this is your
    first time on |Balena|. You will never have to do it again.

Start by :ref:`logging in on the cluster <connect_to_Balena>` if you are not on
it already: you can proceed the same way as you did earlier with 
``linux.bath.ac.uk`` but this time you need to connect to ``balena.bath.ac.uk``.

You first need to setup your account once for all:

.. code-block:: console

    $ cp /beegfs/scratch/group/ce-molsim/brc ~/.bashrc
    $ source ~/.bashrc

The best is to copy and paste those lines: :file:`~/.bashrc` needs to be typed
exactly as it is, with the tilde (``~``), slash (``/``)  and dot
(``.``) followed by ``bashrc``, without spaces.
Once this is done, you are ready to go.


Scratch space
-------------

Among the files and folders that might already exists in you home directory, 
you should find one folder named
:file:`scratch`. 

That :file:`scratch` folder is a very special storage space: it is backed
by a big array of fast hard-drives. This
is where you should do **all** your calculations, without exception.

Contrary to your home directory, your scratch space is not backed up in 
principle.



Preparing the simulation
------------------------

Download the required files from this link: :dl:`water.tar.xz
<data/water.tar.xz>` on your local computer.

.. only:: latex

    .. hint:: The reference to ``water.tar.xz`` above is in fact a 
      :smallcaps:`pdf` attachment; right-click on it and select the option
      to save the file in the drop-down menu.

Copy it over to your home folder on |Balena| using one of the methods presented
in :ref:`file_sharing`.


Create folders as you see fit within your scratch space to contain all the
files related to this water simulation (e.g. :file:`~/scratch/tutorials/gromacs`).
Then move (``mv``) :file:`water.tar.xz` to that folder, change directories
(``cd``) to reach the file and uncompress the archive:

.. code-block:: console

    $ tar xf water.tar.xz

Have a look at the uncompressed
files. The most important one is called :file:`grompp.mdp` as it contains key
parameters for running the |MD| simulation. Have a look at it using ``less``.
The most important parameters for now are shown in
:numref:`water_md_ctrl_params`. These parameters specify that the type of
simulation we perform is molecular dynamics using the *velocity verlet* 
algorithm (``integrator = md-vv``), that the time step is 
``dt = 0.002``\ |~|\ ps and that we will run the simulation for
``nsteps = 50000`` time steps.


.. _water_md_ctrl_params:

.. literalinclude:: data/water/grompp.mdp
    :language: none
    :caption: :file:`water/grompp.mpd`
    :lines: 12-17

The simulation is here performed at a constant temperature of 
``ref_t = 300``\ |~|\ K and constant pressure of
``ref_p = 1.0``\ |~|\ bar as shown in :numref:`water_md_init_cond_params`.

.. _water_md_init_cond_params:

.. literalinclude:: data/water/grompp.mdp
    :language: none
    :caption: :file:`water/grompp.mpd`
    :lines: 90-92,96-99

.. :numref:`water_md_init_vel_params`
.. 
.. .. _water_md_init_vel_params:
.. 
.. .. literalinclude:: data/water/grompp.mdp
..     :language: none
..     :caption: :file:`water/grompp.mpd`
..     :lines: 101-104

.. note:: Lines starting with ``;`` in that files are so-called *comments* or
    *commented lines*: they are lines that are completely ignored by |Gromacs|;
    they are used to convey human-readable information; in these examples they
    are used to express what each variable is about and in what units.

Another interesting file is :file:`conf.gro` that describes the initial
configuration of 216 water molecules: you can visualise it either by using
|Vmd| as described in the :ref:`command line primer
<CLPrimer_exercices_gromacs>` tutorial or by installing a suitable program
(such as |Vmd|_ or |Crystalmaker|_) on your own computer.

.. _Crystalmaker: http://www.crystalmaker.com/crystalmaker/download/index.html
.. _Vmd: http://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=VMD


Running the simulation
----------------------

Running a simulation on |Balena| is a convoluted process. You may have a quick
glance at :ref:`job_submission` to get the general idea but you are not 
expected to understand all these intricacies by yourself: we are going to
get there little by little, a step at a time.

For now, the single thing you need to understand is that running simulations on
|Balena|, directly in the command line, is **strictly forbidden**. When you are
on |Balena|, you log onto the *front node*. This is used for general accessing
of files and folders, but not anything which requires great computing power.
We are going to instead, log onto a *computing node* using a built-in command:

.. code-block:: console

    $ sint

.. note::

   If you are a project student working on this in the tutorial week, you should
   use the following command **instead** of the previous one for the duration
   of the week:

   .. code-block:: console

       $ sint_tutorial

   From now on, we will always refer to ``sint`` when logging on computing
   nodes: you should replace it with ``sint_tutorial`` yourself if applicable.



If logging on the node was successful, you should see a green ``computing``
appear on the prompt. To make |Gromacs| available to you, you need to load it:

.. code-block:: console

    $ module load gromacs/2019.0

A new command, ``gmx_mpi`` should now be available.
This command is used to run |Gromacs| on the cluster. 

Now you are finally all set up to run calculations. First you need to have
|Gromacs| pre-process the input files:

.. code-block:: console

    $ mpiexec -n 1 gmx_mpi grompp -v

This is something you need to do every time you change anything in your input
files (for instance if you change the pressure or the temperature of the
simulation). Once that pre-processing is finished (hopefully without errors),
it is time to start the actual |MD| simulation:

.. code-block:: console

    $ mpiexec gmx_mpi mdrun -v

Obviously, both these commands needs to be used in the directory where you
placed your input files.

Visualising the dynamics with |Vmd|
-----------------------------------

You can also use |Vmd| to visualize individual configurations of the
system as well as the complete run. For this follow these steps:

* Copy the file :file:`spc216.pdb` (which contains the initial configuration)
  and the file :file:`traj_comp.xtc` somewhere on your computer.
* Access ``linux.bath.ac.uk`` and start |Vmd|.
* One of the windows that will open will be called ``VMD Main``. Drag
  and drop the :file:`spc216.pdb` file into the grey area of the window.
  This should load the initial configuration of 216 water molecules.
* Play around with this configuration using various visualization
  options from the ``VMD Main`` window.
* In the ``VMD Main`` window click on the line in the main grey area
  specifying the loaded 216 water molecule configuration. Click 
  :menuselection:`File --> Load data into Molecule` and drop 
  :file:`traj_comp.xtc` into the new window.

This should create a movie-like visualization of the performed |md| simulation.
Play with it (i.e. move back and forward along time axis, rotate the
system, zoom. change colours, etc.).

Analysing the data
------------------

Now is the time to analyse the results we obtained from the previous 
simulation. We could start by analysing the trajectory file 
(:file:`traj_comp.xtc`) which contains the positions of water molecules at
each step of the calculation.


|Gromacs| provides a post-processing command dedicated to generating
various data about hydrogen bonds in the system. In particular, it will give
you the average number of hydrogen bonds per molecule of water:


.. code-block:: console

    $ mpiexec -n 1 gmx_mpi hbond -f traj_comp.xtc -xvg none

Select the group you are interested in and plot the resulting number of 
hydrogen bonds using ``gnuplot`` (:file:`hbnum.xvg`):

.. code-block:: console

    $ gnuplot -p -e 'plot "hbnum.xvg" with lines'


It is also possible to calculate the self-diffusion coefficient of water:

.. code-block:: console

    $ mpiexec -n 1 gmx_mpi msd -f traj_comp.xtc -xvg none

Again, choose the group you are interested in and plot the result.

Finally, energy analysis is also possible:

.. code-block:: console

    $ mpiexec -n 1 gmx_mpi energy -f ener.edr -xvg none

In contrast to the other three commands, the energy program analyses the energy
file. It will offer you a choice of different properties such as density,
potential energy, etc. It reports the average of this characteristic over the
simulation time but also saves it in a file (:file:`energy.xvg`) as a function
of simulation time. Regardless of the chosen property, the generated file is
always the same :file:`energy.xvg`. So if you want to keep different output
files make sure that you rename the file or it will be overwritten! Units
should be reported in the output.

.. hint:: :prog:`Excel` is also an excellent tool to plot the
    the content of those files. You can remove the ``-xvg none`` flag from the
    above commands to get a more verbose and detailed version of the output. 
    This detailed output cannot be plotted with ``gnuplot``.




Investigating water properties
------------------------------

Now that you have run your first molecular dynamics simulations and processed
the data, it's time now to put these skills into practice. The force field we
are using can represent the phase diagram of water fairly well but has its
limits ---let's test its boundaries! We can do this by running simulations at
different temperatures, from :math:`300\,\mathrm{K}` to :math:`140\,\mathrm{K}`
in :math:`40\,\mathrm{K}` steps and by following the thermodynamic properties
of each simulation, we can work out at what temperature the force field predicts
the phase transition from water to ice occurs. Follow these steps to
test it out!

* Go *up* a level in your file hierarchy and make a sub folder for *each* 
  simulation temperature *e.g.*

  .. code-block:: console

      $ cp -r water water_300K

* Go into each directory to change the temperature accordingly in
  :file:`grompp.mpd` (``ref_t`` variable, see
  :numref:`water_md_init_cond_params`)

* Perform the simulations in each sub folder, as you did the first time
  around, by changing into the desired directory and using the two commands for
  executing |Gromacs| |MD| simulations:

  .. code-block:: console

      $ mpiexec -n 1 gmx_mpi grompp -v
      $ mpiexec gmx_mpi mdrun -v
	
* Use the data analysis tools to watch how the properties of water change with
  temperature.
* Plot the average self-diffusion coefficient against temperature.
* Plot the number of hydrogen bonds against temperature.
* View your trajectories for each temperature in |Vmd|.

.. hint:: Again, using :prog:`Excel` to plot things is fine.

Questions
---------

* The bulk thermodynamic properties of water at 300 K are:

  * Density of water:  :math:`997\,\mathrm{kg/m}^3`.
  * Diffusion coefficient: :math:`2.4\times10^{-9}\,\mathrm{m}^2\mathrm{/s}`.
  
  Compare your results from your simulation at :math:`300\,\mathrm{K}` with the
  experimental properties of water. How different are they? 
* By analysing your variable temperature water |MD| study, can you work out at
  which temperature water turns from liquid to ice?
* What key parameters change? (Density, hydrogen bonds, diffusion coefficient, etc.) 
* Do you think :smallcaps:`spc` is a good model for water?


