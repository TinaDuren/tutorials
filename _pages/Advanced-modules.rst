
.. _sec_module_management:

How do you access specific programs?
====================================

.. secauthor:: Gaël Donval <g.donval@bath.ac.uk>

|Balena| is used for widely different things by widely different people.
Installing programs system-wide, as you would on your computer, cannot work
because of mutual incompatibilities. On |Balena|, we work around this problem
using a so-called *module* system. From your perspective, almost every time you
need to use a specific program, you need to load the corresponding module
first. 


Listing available modules
-------------------------


There are dozens of different modules readily accessible on |Balena|.
You can list the modules currently loaded (i.e. in use) like so:

.. code-block:: console

   $ module list

You can also list all the modules available on the cluster using:

.. code-block:: console

   $ module avail

Use these commands as much as you want: if through the tutorials, the computer
is complaining that a command is unknown, it is very likely because you 
forgot to load the necessary modules.


Loading modules
---------------

This is done by typing:

.. code-block:: console

   $ module load <module name or list>

All the program you will need are available in a special module repository
loaded and displayed by typing the following commands:

.. code-block:: console

   $ module purge
   $ module load group ce-molsim stack
   $ module avail

You should now see a list of modules. The last section should contain 
something like ``music/std``: this is one version of the |Music| program that
some of you are going to use. |Music| can be invoked by calling:

.. code-block:: console

    $ music_gcmc --help

|Bash| should complain that ``music_gcmc`` does not exist: the relevant module
needs to be loaded first...

.. code-block:: console

    $ module load music/std
    $ music_gcmc --help

.. note:: Modules are loaded on a per-session basis: you basically need to 
        load the requested modules every single time you connect to |Balena|. 



Unloading modules
-----------------

Though unloading modules is possible, it is not recommended. It is too easy 
to lose track of which modules are loaded or not. It is better to
purge the module list to start afresh instead:

.. code-block:: console

   $ module purge
   $ module load group ce-molsim stack

.. code-block:: console

    $ music_gcmc --help
    bash: music_gcmc: command not found

