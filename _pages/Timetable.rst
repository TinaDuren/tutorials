
.. only:: html

    *********
    Timetable
    *********


    .. tabularcolumns:: llll

    .. _idx_schedule:


    ============== ========  ======  ============================================
    **Monday**

    10:15--11:05   1W 2.101  MJL     Induction lecture & health and safety
    11:15--13:05   8W 2.22   CH/GD   Lecture: intermolecular interaction
                                     & molecular dynamics (:smallcaps:`md`)
    14:15--16:15   9W 3.07   TD      Tutorial: :ref:`command-line on Linux
                                     <sec_CLP>`

    **Tuesday**

    10:15--12:05   9W 3.07   TD      Tutorial: :ref:`phase diagram of water
                                     <water_in_gromacs>`
    16:15--18:05   CB 4.17   DS      Lecture: literature search

    **Wednesday**

    11:15--12:05   8W 2.22   SW      Lecture: |MC| (:smallcaps:`mc`) simulations

    **Thursday**

    10:15--12:05   9W 3.07   CH      Tutorial: :ref:`solvation of TNT in water
                                     <tnt_in_gromacs>`
    13:15--15:05   9W 3.07   MJL     Tutorial: :ref:`Henry's coefficients
                                     <henry_in_raspa>`

    **Friday**

    10:15--11:05   9W 3.07   TD      Ask you questions about molecular simulation
    11:15--12:05   8W 1.33           Group meeting
    14:15--16:05   9W 3.07   MJL     Tutorial: :ref:`comparing the performance
                                     of two porous materials for carbon capture
                                     <ads_in_music>`
    ============== ========  ======  ============================================

